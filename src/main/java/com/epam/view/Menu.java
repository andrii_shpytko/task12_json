package com.epam.view;

import java.util.Map;
import java.util.Scanner;

public interface Menu {

    String initMenuName();

    Map<String, String> initDisplay();

    Map<String, Runnable> initExecution();

    default void launch() {
        String name = initMenuName();
        Map<String, String> items = initDisplay();
        Map<String, Runnable> menu = initExecution();
        startMenu(name, items, menu);
    }

    default void startMenu(String name, Map<String, String> items, Map<String, Runnable> menu) {
        Scanner input = new Scanner(System.in);
        String keyMenu;

        do {
            System.out.println("\n" + name);
            items.values().forEach(item -> System.out.println(" " + item));

            System.out.print("\nSelect menu item: ");
            keyMenu = input.nextLine().toUpperCase();

            try {
                menu.get(keyMenu).run();
            } catch (NullPointerException e) {
                System.out.println("\ninvalid input; please re-enter");
            }
        } while (!keyMenu.equals("Q"));
    }
}
