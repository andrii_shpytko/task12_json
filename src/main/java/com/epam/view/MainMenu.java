package com.epam.view;

import com.epam.controller.Controller;

import java.util.LinkedHashMap;
import java.util.Map;

public class MainMenu implements Menu {

    private Controller controller;

    public MainMenu(Controller controller) {
        this.controller = controller;
    }

    @Override
    public String initMenuName() {
        return "MENU";
    }

    @Override
    public Map<String, String> initDisplay() {
        return new LinkedHashMap<String, String>() {{
            put("1", "1 - Parse from file Jackson");
            put("2", "2 - Parse from file GSON");
            put("3", "3 - Check JSON schema validation");
            put("Q", "Q - exit");
        }};
    }

    @Override
    public Map<String, Runnable> initExecution() {
        return new LinkedHashMap<String, Runnable>() {{
            put("1", () -> controller.getTouristVouchersJackson("touristVouchers"));
            put("2", () -> controller.getTouristVouchersGson("touristVouchers"));
            put("3", () -> controller.checkSchemaValidation("touristVouchers", "touristVouchersSchema"));
            put("Q", () -> System.out.println("\nBye"));
        }};
    }
}
