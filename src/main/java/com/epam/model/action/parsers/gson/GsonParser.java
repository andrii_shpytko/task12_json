package com.epam.model.action.parsers.gson;

import com.epam.model.action.Parser;
import com.epam.model.entity.TouristVoucher;
import com.google.gson.Gson;

import java.io.FileNotFoundException;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class GsonParser extends Parser {

    public GsonParser(String fileName) throws FileNotFoundException {
        super(fileName);
    }

    @Override
    public List<TouristVoucher> parseFromFile() throws Exception {
        Optional<String> data = Files.lines(getJson().toPath()).reduce(String::concat);
        return Arrays.asList(new Gson().fromJson(data.orElseThrow(RuntimeException::new), TouristVoucher[].class));
    }
}
