package com.epam.model.action.parsers.jackson;

import com.epam.model.action.Parser;
import com.epam.model.entity.TouristVoucher;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.List;

public class JacksonParser extends Parser {

    public JacksonParser(String fileName) throws FileNotFoundException {
        super(fileName);
    }

    @Override
    public List<TouristVoucher> parseFromFile() throws Exception {
        return Arrays.asList(new ObjectMapper().readValue(getJson(), TouristVoucher[].class));
    }
}
