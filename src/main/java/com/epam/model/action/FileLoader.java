package com.epam.model.action;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.URL;

public class FileLoader {

    public File getFile(String fileName) throws FileNotFoundException {
        URL url = ClassLoader.getSystemClassLoader().getResource(fileName);
        if (url == null) {
            throw new FileNotFoundException(fileName + " not found");
        }
        return new File(url.getFile());
    }
}
