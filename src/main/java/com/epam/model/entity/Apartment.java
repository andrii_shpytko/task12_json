package com.epam.model.entity;

public class Apartment {

    private int idNumber;
    private int quantityPerson;

    public Apartment() {}

    public Apartment(int idNumber, int quantityPerson) {
        this.idNumber = idNumber;
        this.quantityPerson = quantityPerson;
    }

    public int getIdNumber() {
        return idNumber;
    }

    public int getQuantityPerson() {
        return quantityPerson;
    }

    @Override
    public String toString() {
        return "Apartment{" +
                "idNumber=" + idNumber +
                ", quantityPerson=" + quantityPerson +
                '}';
    }
}
