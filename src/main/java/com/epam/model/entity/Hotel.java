package com.epam.model.entity;

public class Hotel {

    private String hotelName;
    private double hotelRating;
    private Apartment apartment;
    private HotelService hotelService;

    public Hotel() {}

    public Hotel(String hotelName, double hotelRating, Apartment apartment, HotelService hotelService) {
        this.hotelName = hotelName;
        this.hotelRating = hotelRating;
        this.apartment = apartment;
        this.hotelService = hotelService;
    }

    public String getHotelName() {
        return hotelName;
    }

    public double getHotelRating() {
        return hotelRating;
    }

    public Apartment getApartment() {
        return apartment;
    }

    public HotelService getHotelService() {
        return hotelService;
    }
}
