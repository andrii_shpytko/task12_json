package com.epam.model.entity;

public class TouristVoucher {

    private int id;
    private String type;
    private String country;
    private int quantityDays;
    private int quantityNights;
    private Transport transportType;
    private Hotel hotel;

    public TouristVoucher() {}

    public TouristVoucher(int id, String type, String country,
                          int quantityDays, int quantityNights, Transport transportType, Hotel hotel) {
        this.id = id;
        this.type = type;
        this.country = country;
        this.quantityDays = quantityDays;
        this.quantityNights = quantityNights;
        this.transportType = transportType;
        this.hotel = hotel;
    }

    public int getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    public String getCountry() {
        return country;
    }

    public int getQuantityDays() {
        return quantityDays;
    }

    public int getQuantityNights() {
        return quantityNights;
    }

    public Transport getTransportType() {
        return transportType;
    }

    public Hotel getHotel() {
        return hotel;
    }

    @Override
    public String toString() {
        return "TouristVoucher id: " + id;
    }
}
