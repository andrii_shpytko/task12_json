package com.epam.model.entity;

public enum Transport {
    car,
    bus,
    train,
    airplane,
    ship
}
