package com.epam.model.entity;

public class HotelService {

    private boolean meal;
    private boolean tv;
    private boolean conditioner;

    public HotelService() {}

    public HotelService(boolean meal, boolean tv, boolean conditioner) {
        this.meal = meal;
        this.tv = tv;
        this.conditioner = conditioner;
    }

    public boolean isMeal() {
        return meal;
    }

    public boolean isTv() {
        return tv;
    }

    public boolean isConditioner() {
        return conditioner;
    }
}
