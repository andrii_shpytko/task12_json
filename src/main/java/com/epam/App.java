package com.epam;

import com.epam.controller.Controller;
import com.epam.view.MainMenu;

/**
 * Hello world!
 */
public class App {
    public static void main(String[] args) {
        System.out.println("Hello World!");
        new MainMenu(new Controller()).launch();

    }
}
