package com.epam.controller;


import com.epam.model.action.Parser;
import com.epam.model.action.parsers.jackson.JacksonValidator;
import com.epam.model.entity.TouristVoucher;

import java.util.Comparator;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Controller {
    private static final Logger LOGGER = LogManager.getLogger(Controller.class);

    public void getTouristVouchersJackson(String fileName) {
        try {
            List<TouristVoucher> touristVouchers = Parser.parserFromFile("Jackson", fileName);
            LOGGER.info("returned list of TouristVouchers (Jackson parser)");
            logTouristVouchers(touristVouchers);
        } catch (Exception e) {
            LOGGER.info("unable to get list of TouristVouchers (Jackson parser); " + e.getMessage());
        }
    }

    public void getTouristVouchersGson(String fileName) {
        try {
            List<TouristVoucher> touristVouchers = Parser.parserFromFile("GSON", fileName);
            LOGGER.info("returned list of TouristVouchers (GSON parser)");
            logTouristVouchers(touristVouchers);
        } catch (Exception e) {
            LOGGER.info("unable to get list of TouristVouchers (GSON parser); " + e.getMessage());
        }
    }

    public void checkSchemaValidation(String fileName, String schemaName) {
        try {
            boolean result = JacksonValidator.validate(fileName, schemaName);
            LOGGER.info("JSON file " + fileName + " has been validated by " + schemaName + "; " +
                    "result [" + result + "]");
        } catch (Exception e) {
            LOGGER.info("unable to check schema validation; " + e.getMessage());
        }
    }

    private void logTouristVouchers(List<TouristVoucher> touristVouchers) {
        touristVouchers.sort(Comparator.comparingInt(TouristVoucher::getId));
        touristVouchers.forEach(touristVoucher -> {
            LOGGER.info("TouristVoucher ID: " + touristVoucher.getId());
            LOGGER.info("Type: " + touristVoucher.getType());
            LOGGER.info("Country: " + touristVoucher.getCountry());
            LOGGER.info("quantityDays: " + touristVoucher.getQuantityDays());
            LOGGER.info("quantityNights: " + touristVoucher.getQuantityNights());
            LOGGER.info("transportType: " + touristVoucher.getTransportType());
            LOGGER.info("Hotel");
            LOGGER.info(" hotelName: " + touristVoucher.getHotel().getHotelName());
            LOGGER.info(" hotelRating: " + touristVoucher.getHotel().getHotelRating());
            LOGGER.info(" Apartment");
            LOGGER.info("  idNumber: " + touristVoucher.getHotel().getApartment().getIdNumber());
            LOGGER.info("  quantityPerson: " + touristVoucher.getHotel().getApartment().getQuantityPerson());
            LOGGER.info(" hotelService");
            LOGGER.info("  Meal: " + touristVoucher.getHotel().getHotelService().isMeal());
            LOGGER.info("  TV: " + touristVoucher.getHotel().getHotelService().isTv());
            LOGGER.info("  Conditioner: " + touristVoucher.getHotel().getHotelService().isConditioner());
        });
    }
}
